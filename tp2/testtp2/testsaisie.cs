﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using tp2;

namespace testtp2
{
    [TestClass]
    public class Testsaisie
    {
        [TestMethod]
        public void TestMaj()
        {
            Assert.AreSame("true",Framboise.Majuscule("Sympa"));
            Assert.AreSame("false", Framboise.Majuscule("syMpa"));
            Assert.AreSame("false", Framboise.Majuscule("&"));
            Assert.AreSame("false", Framboise.Majuscule("8"));

        }
        [TestMethod]
        public void TestFin()
        {
            Assert.AreSame("true", Framboise.Fin("Sympa ça ."));
            Assert.AreSame("false", Framboise.Fin("sympa ça"));
            Assert.AreSame("false", Framboise.Fin("sympa ça &"));
            Assert.AreSame("false", Framboise.Fin("sympa . ça"));
            Assert.AreSame("false", Framboise.Fin("sympa ça 5"));

        }
        [TestMethod]
        public void TestSaisieInt()
        {
            Assert.AreSame("true",  Tableau.Saisie("25"));
            Assert.AreSame("false", Tableau.Saisie("&"));
            Assert.AreSame("false", Tableau.Saisie("A"));
            Assert.AreSame("false", Tableau.Saisie("!"));

        }



    }
}
